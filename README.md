# Assignment 2

### INF 226
### Author - Runar Fosse

------

# 2A

## <i> The code is badly structured. What's wrong with the structure? Could any of these have a security impact? </i>

The first thing that is wrong with the structure is that it doesn't follow flask's "standard" project structure setup ([as seen here](https://flask.palletsprojects.com/en/2.2.x/patterns/packages/)). This makes it hard for other developers to e.g. recognize the connection between files, modify the structure or analyze for security flaws. It will also make it very hard to extend the project with new implementations.

These things make the probability of security flaws staying unnoticed much higher than it would be in a properly structured project.

In this project there also exist unused python files. These do not include code which could open for any security flaws, but one could easily see how these, later down the line (of a hypothetical project), could open for backdoor injections etc.

The whole messaging app itself is (relatively) littered with security flaws:


The login system does not verify passwords, only checks for an existing username.

Searching through messages can be misused by SQL injections. 
This can be done by e.g. entering the string: <br>```'; UPDATE messages SET message='Hacked' WHERE sender='Alice'; --```

Sending messaged can be misused by XSS. 
This can be done by e.g. sending the message: <br> ```<span onclick=alert(1)> hacked </span>``` <br> where alert(1) can be changed out with other malicious code.

Another security issue is that there is no way of seeing who has sent a message. A message can only be traced back to its sender, which currently is a username which every user can change to be whatever they want. This means there are no accountability, such that people who misuse the service cannot be traced.

Lastly, this messaging system is also vulnerable to CSRF attacks, as there are no CSFR token checks (which for example are gotten for free with wtforms in the login form). This means that an end-user can be tricked by clicking a form on another website, redirecting messages to our server. In addition to this, the send() method (in app.py) also supports POST and GET requests, meaning a user can be tricked in clicking a link, which will send a message to this server. This can be done by e.g. clicking the link: <br> ```localhost:5000/send?sender=Alice&message=Hacked``` <br> (this is because clicking links usually are GET, and not POST requests)

-------

# 2B

## <i> Design considerations </i>

In short, I've tried to change this boilerplate project into a secure messager app with features that make sense in such an app.

First I started by cleaning up the project structure. If there does not exist any structure within the project, it'll be very hard for me (and others) to get a clear overview of the project itself and all its components. I followed a [standard flask project structure](https://flask.palletsprojects.com/en/2.2.x/patterns/packages/), such that every static file is under "/static" (this will be icons, css files, js files etc), every template file is under "/templates" (html template files) and the python files exist in the top folder.

I fully implemented a secure user system, where users can create an account on the signup page. This will store their username, aswell as a secure hash of their password, in the database under the table "users". On the login page a user can enter their username and password, the username will be used to get the password hash from the database, which will be used to check if the input password matches. Password hashes are also salted to prevent rainbow table attacks.
Every username has to be unique, and every password has to be of (at least) length 8, with at least 1 capital letter and 1 numerical character. This will make bruteforcing passwords harder for adversaries as passwords will in general be more complex.

I then started implementing the messaging application itself. I wanted this app to represent a "public" forum where everyone can see what everyone else writes. You also have the possibility to send a private message to a single other user. 

When you load into the message app, every message you are able to view will get loaded onto the site. No html which might exist in the messages will be parsed, such that we prevent XSS attacks from happening.
The website synchronizes with the server every 1/2 second to ensure that every new message gets loaded relatively fast.
Private messages are sent between 2 users, and only these 2 users will be able to retrieve those said messages from the server.

When you send a message, the CSRF token is compared to ensure that this POST request has not come from somewhere else. This happens automatically when using the signup and login forms (thanks to wtforms), but has to be done manually when sending messages.

Another problem which have been solved is ensuring the tracability of a message through the sender being correct. Whenever a message has been sent (and after verifying the CSRF token), we add the message's sender as the user which is currently logged in. This not only ensures tracability but also accountability, as a message of a given sender is guaranteed to be from that actual sender (in comparison to the case in 2A).

Every SQL query (which depend on user input) use prepared statements to prevent SQL injection from happening. I have also removed the "search" functionality of the boilerplate application, as this did not make much sense in having in a message application.

The secret key of the application is also randomized (securely) every time the flask application is run, such that if you restart the server, everyone's session cookie invalid and therefore everyone will be logged out.

## <i> Features of this application </i>

### Application description

This application represents a basic messaging forum, where every user created can read every message sent publicly. If you don't want to send your message to everyone, then private messages between two users is also possible.

### Creating a user

Through the "/signup" page you can create a user, with a unique username, and a password which contains at least 1 capital letter, 1 numerical character and is of at least length 8.

### Logging in

Through the "/login" page you can enter your username and password, and you will automatically be logged in and redirected to the message application. Be cautious of that the "/login" page will always redirect you back to the application if you are already logged in.

### Message application

After logging in you will automatically be redirected to the application, however you can also access it through "/" or "/index.html". Here every message sent before will automatically be loaded in. You can talk to other people through the text-box on the bottom, and every message sent will automatically pop up on other people's open applications.

If you want to send a private message, you need to enter the command "/private [recipient] [message]" in the text-box, where [recipient] is the user you want to send it to, and [message] being the message you want to send. This private message will only be shown to you and the recipient, no other people will be notified of your message existing.

### Logging out

If you want to log out of the application you can enter the "/logout" page, our click the "log out" button on the top left of the application. After logging out you will automatically be redirected to the login screen.

## <i> Instructions on how to demo/test it </i>

The application is run with the command 'flask run', this will set up everything for you. Then go to 'localhost:5000' and you will be redirected to the login site.

When starting the application for the first time, there already exists 2 users, "alice" and "bob", with the respective passwords "password123" and "bananas". If you want to create your own user, you can use the "/signup" page. Remember that creating a user will send both the entered username and password to the server as plaintext, however will only store the password in the database as a salted hash.

When logging in, you need to enter the username and password of a existing user into the form. When entering the password, ensure that the capitalization is correct. This is not needed when entering the username.

Testing the messaging system is straight forward, and works as an actual messaging system would. Remember that no message will be added directly into SQL queries, nor directly parsed as html, such that SQL injection and XSS attacks shouldn't work (but remember to test it nevertheless). 

## <i> Technical details of the implementation </i>

This application basically uses all the technology given in the boilerplate project, [login-server](https://git.app.uib.no/inf226/22h/login-server).
However, for the hashing of passwords, the bcrypt library has been used. For randomizing the app.secret_key, the secrets library has been used. For handling login and logout, flask_login has been used. At last, for manual CSRF verification, the flask_wtf.csrf.CSRFProtect has been used.

## <i> Answer to the questions </i>

* Threat model - who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

There are many security flaws within the original boilerplate application.

If we assume that this boilerplate applictation is a messaging system which is currently in use somewhere, at a company or by private people etc, there could be several different types of attackers. Some want to read messages sent within the application or extract other data from the database, to maybe uncover classified or sensitive information. Some attackers may try to change the information already within the application. Other might try to prevent the application from working.

There is a lot an attacker currently can do in this application, due to the security flaws.

Firstly, the "search" functionality is open to SQL injections. This could lead to attackers extracting everything from the database. Currently, this is limited to messages and announcements in the boilerplate application. They can also change the information, or delete information. 

There are also XSS possibilities, such that you could also trick users into clicking message content which could run harmful scripts. There are also no CSRF prevention within the messaging code, such that this is also a risk.

These things together can do alot of damage. The SQL injection alone can breach all three elements in the CIA triad. Confidentiality is breached if the attackers extract sensitive or confidential information from the database. Integrity is breached if the attackers change data which is in the database. And availability is breached if the attackers delete information from the database (or even the whole database itself).
From XSS and CSRF you also breach authenticity and accountability, as messages sent from a given person might actually be from an attacker.

Lastly, the login functionality only requires an existing username, as it does not check the password. This is called broken authentication and is another security issue of the boilerplate application.

When asking if there are limits to what an attacker can do, the answer is yes. This boilerplate application is very simple, and only stores messages and announcements in the database. This means that the only thing attackers actually can manipulate is surrounding this. 

There are also limits to what we sensibly can protect against, as we usually are only able to protect against attacks we already recognize. As talked about above, this might include attacks such as SQL injection, XSS and CSRF. However, when adding more features into the application, more security issues might arise, which means we have to protect against even more. E.g. before implementing private messages, there are no reason to not let every user be able to access every message sent.

* What are the main attack vectors for the (boilerplate) application?

As talked about above, the main attack vector is through the searching and sending system within the messaging application. These open up the possibility for SQL injection, XSS and CSRF attacks. 

* What should we do (or what have you done) to protect against attacks?

Firstly we should protect against these three big vulnerabilities. SQL injection is prevented through the use of prepared statements when entering raw user input into SQL queries. I have also in the application removed the ability to search through the database (as it didn't make sense to have in a messaging app). 

XSS attacks should be prevented, which I have done through not parsing user input as HTML, but rather insert it into HTML elements through innerText. 

CSRF attacks should also be prevented, and have been through appending a CSRF token to the '/send' POST request, which is verified to be the correct one on the server. This means messages can not be sent in the application without the correct CSRF token corresponding to the current logged in user.

After implementing login (with verification of passwords) there was sensitive user data stored in the database, notibly passwords. Storing passwords as plaintext on the database is never a good idea, such that hashing them was a no-brainer. To make the storing of this sensitive data even more secure, I also implemented salting of the hashed passwords, preventing rainbow table attacks.

* What is the access control model?

In the boilerplate application is supposed to have *Mandatory Access Control*, where users should be have permission to do everything in the application, whilst people with out a user probably should not. Since the authentication is very broken, and the website does not require you to be logged in to do any of these, it thus in practice doesn't have any access control.

In my application, this *Mandatory Access Control* does exist, as there is a difference of being logged in vs out. Every user can in general read every message, but now there also exist private messages. These prevent every user except two from having access and reading the contents. This access control model is called *Discretionary Access Control*, where only the owner and a given subject/user has access. 

* How can you know that your security is good enough?

"Good enough" security is hard to define, but I would define it as minimizing the attack surface such that most/all of the most common vulnerabilities are covered, prevent such attacks as SQL injection, XSS etc. However, since there always might exist security flaws within every application, traceability is an important feature. This means that if there eventually was a security breach, you can trace its origins and try to prevent more of such attacks from happening. This traceability property can be obtained from logging different activity within the application.