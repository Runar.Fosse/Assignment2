from flask import Flask
from flask_wtf.csrf import CSRFProtect
import sys
import apsw
from apsw import Error
from bcrypt import gensalt, hashpw
import secrets

# Setup app
app = Flask(__name__, template_folder="templates")
# The secret key enables storing encrypted session data in a cookie
app.secret_key = secrets.token_bytes(24)

# Enable CSRF protection (when not using wtforms)
csrf = CSRFProtect(app)

# Connect to the database (and add default structure)
try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        recipient TEXT NOT NULL,
        message TEXT NOT NULL,
        timestamp DATETIME NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY, 
        username TEXT NOT NULL,
        password TEXT NOT NULL);''')    

    # Add sample users if user table is empty
    if c.execute('SELECT COUNT(*) FROM users').fetchone()[0] == 0:
        c.execute(f'INSERT INTO users (username, password) VALUES ("alice", "{hashpw("password123".encode("utf-8"), gensalt()).decode("utf-8")}"), ("bob", "{hashpw("bananas".encode("utf-8"), gensalt()).decode("utf-8")}")')
        c.execute(f'INSERT INTO messages (sender, recipient, message, timestamp) VALUES ("alice", "everyone", "Hello Bob!", "2022-29-10 10:46:54"), ("bob", "everyone", "Hello Alice!", "2022-29-10 11:26:32")')
except Error as e:
    print(e)
    sys.exit(1)

import views