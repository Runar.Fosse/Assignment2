from app import app, conn

from http import HTTPStatus
from werkzeug.datastructures import WWWAuthenticate
from base64 import b64decode

from flask import abort
import bcrypt

# Add a login manager to the app
import flask_login
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass

# Checks that a password follows the rules given (i.e. it's secure)
def check_password_rules(password):
    # If password is shorter than 8
    if len(password) < 8:
        return False
    
    # Need at least 1 capital letter
    if len(list(filter(lambda c: c.isupper(), password))) < 1: 
        return False

    # Need at least 1 number
    if len(list(filter(lambda c: c.isdigit(), password))) < 1:
        return False

    return True

# Check if a password matches the hash
def check_password(input_password, hashed_password):
    return bcrypt.checkpw(input_password.encode("utf-8"), hashed_password.encode("utf-8"))

# Hashes a password
def hash_password(password):
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(password.encode("utf-8"), salt)
    return hashed

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):    
    # Return if user does not exist
    if conn.cursor().execute(f'SELECT COUNT(*) FROM users WHERE username=?', (user_id,)).fetchone()[0] == 0:
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
        user_exists = conn.cursor().execute(f'SELECT 1 FROM users WHERE username=?', (uid,)).fetchone()
        if user_exists:
            user_password = conn.cursor().execute(f'SELECT 1 FROM users WHERE username=?', (uid,)).fetchone()[0]
            if check_password(user_password, passwd):
                return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        user_tokens = conn.cursor().execute('SELECT token FROM users').fetchall()
        for (token) in user_tokens:
            if token == auth_params:
                return user_loader(uid)
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))