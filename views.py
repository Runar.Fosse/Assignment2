from users import hash_password
from app import app, conn, csrf
from users import user_loader, check_password, check_password_rules

import flask
from flask import abort, request, send_from_directory, make_response, render_template
from forms import LoginForm, SignupForm
from flask_login import login_required, login_user, logout_user, current_user, decode_cookie
from apsw import Error

from datetime import datetime
timestamp_format = '%Y-%m-%d %H:%M:%S'

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'static/favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'static/favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return render_template('index.html')

# Redirect to login screen if not logged in
@app.login_manager.unauthorized_handler
def unauthorized_callback():
    return flask.redirect('/login')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    error = ""
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data
        password_confirm = form.password_confirm.data

        user_exists = conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (username,)).fetchone()

        if user_exists or username == "everyone":
            error = f'Username {username} already exists.'

        elif password != password_confirm:
            error = 'Passwords do not match.'

        elif not check_password_rules(password):
            error = 'Password does not follow the rules.'

        else:
            password_hash = hash_password(password)
            conn.cursor().execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, password_hash.decode("utf-8")))
            error = f"User {username} has been created."
            return flask.redirect(flask.url_for('login'))

    return render_template('signup.html', form=form, error=error)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated: # Dont log in if user is already logged in
        return flask.redirect(flask.url_for('index_html'))

    form = LoginForm()
    error = ""
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data

        user_exists = conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (username,)).fetchone()

        if user_exists:
            password_hash = conn.cursor().execute('SELECT password FROM users WHERE username=?', (username,)).fetchone()[0]

        if user_exists and check_password(password, password_hash):
            user = user_loader(username)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            return flask.redirect(flask.url_for('index_html'))
        else:
            error = "Wrong username or password"
    return render_template('login.html', form=form, error=error)

@app.get('/logout')
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('login'))

@app.route('/new', methods=['POST'])
@login_required
def send():
    try: 
        # This isn't protected by wtforms so we need to do csrf validation ourselves
        if request.headers.get('CSRFTOKEN') != csrf._get_csrf_token():
            return '{"error": "CSRFTOKEN MISMATCH"}'

        sender = current_user.id
        recipient = request.args.get('recipient')
        message = request.args.get('message')
        timestamp = datetime.now().strftime(timestamp_format)
        if not message:
            return '{"error": "Missing message"}'
        
        if not recipient: # If there is no explicit recipient, send message to everyone
            recipient = 'everyone'
        else:
            recipient_exists = conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (recipient,)).fetchone()
            if not recipient_exists: # Check if the recipient exists
                return '{"error": "The recipient does not exist!"}'
            else:
                recipient = recipient.lower()

        if recipient == sender:
            return '{"error": "You can\'t send a private message to yourself!"}'

        # Escape special characters
        message = message.replace('\"', '\\"', -1)

        stmt = 'INSERT INTO messages (sender, recipient, message, timestamp) VALUES (?, ?, ?, ?)'
        conn.execute(stmt, (sender, recipient, message, timestamp)) # Insert the message into the database
        return '{}'
    except Error as e:
        return '{"error":'+f'"{e}"'+'}'

@app.get('/messages/ID')
@login_required
def retrieve():
    try:
        message_id = request.args.get('id')
        if not message_id: # If there is no message ID given, return the highest message ID (used for synchronizing messages in a currently open website)
            max_message_id = conn.cursor().execute("SELECT MAX(id) FROM messages").fetchone()[0]
            return '{"message_id": "'+str(max_message_id)+'"}'

        # Return the message of the given message ID
        message_id, sender, recipient, message, timestamp = conn.cursor().execute('SELECT * FROM messages WHERE id = ? AND (recipient IN ("everyone", ?) OR sender = ?)', (message_id,current_user.id,current_user.id)).fetchone()
        return '{"message_id": "'+str(message_id)+'", "sender": "'+sender+'", "recipient": "'+recipient+'", "message":"'+message+'", "timestamp":"'+timestamp+'"}'
    except Error as e:
        return f'ERROR: {e}'

@app.get('/messages')
@login_required
def retrieve_all():
    try:
        result = conn.cursor().execute('SELECT * FROM messages WHERE recipient IN ("everyone", ?) OR sender = ? ORDER BY id ASC', (current_user.id,current_user.id)).fetchall()
        
        # Return an array of all messages
        messages = ""
        for (message_id, sender, recipient, message, timestamp) in result:
            messages += '{"message_id": "'+str(message_id)+'", "sender": "'+sender+'", "recipient": "'+recipient+'", "message":"'+message+'", "timestamp":"'+timestamp+'"},'
        return '[' + messages[:-1] + ']'
    except Error as e:
        return f'ERROR: {e}'